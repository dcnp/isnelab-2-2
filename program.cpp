#include <iostream>
#include "list.h"
using namespace std;

int main()
{

	List mylist;
	char charInput , searchInput ;

	cout << "please enter some character or 0 to end : ";
	cin >> charInput ; 
	cout << endl ;
	mylist.pushToHead(charInput);
	do{
		cout << "please enter some character or 0 to end : ";
		cin >> charInput ; 
		cout << endl ;
		mylist.pushToTail(charInput);
	}while(charInput != '0');
	
	if(charInput == '0'){
		mylist.popTail() ;
	}
	
	mylist.print();
	cout << endl << endl ;
	
	cout << "What character would you like to search : " ;
	cin >> searchInput ;
	cout << endl ;
	if(mylist.search(searchInput)){
		cout << searchInput << " Found " << endl ;
	}
	else{
		cout << searchInput << " Not Found " << endl ;
	}
	

	//Adapt your code so that your list can store data other than characters - how about a template?
	return 0;
}
