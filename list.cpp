#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for (Node *p; !isEmpty(); ) {
		p = head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head, NULL);
	if (tail == 0)
	{
		tail = head;
	}
	else
	{
		head->next->prev = head;
	}
}
void List::pushToTail(char el)
{
	Node *tmp = new Node(el); //create new node that pointer tmp point to it
	tmp->prev=tail; // make new node point to old tail
	tail->next=tmp; // make old tail point to tmp **now it point to each other	
	tail = tmp ; // move tail to new node	
	tmp -> next = NULL ; //make new tail point to NULL 

}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if (head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
		head->prev = 0;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	char el = tail->data; //Create el to store data in the node that we want to delete
	Node *tmp = tail; // create new pointer and point it to tail
	if (tail == head) {// if tail and head is the same node 
		head = tail = 0; // make these 2 pointers point to 0
	}
	else {
		tail = tail->prev; // move tail pointer to previous node
		tail->next = NULL; //tail node point to NULL
	}
	delete tmp; //delete node

	return el;
} 

bool List::search(char el)
{
	Node *tmp = head;
	while ( tmp != NULL) {  //check value that it is in the list or not
		if (tmp->data == el) {
			return true; // return true if it's found
		}
		else {
			tmp = tmp->next; //if not found move pointer to next node
			
		}
	}		
	return false; //return false when do not found in the list
}


void List::print()
{
	if (head == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while (tmp != tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}
